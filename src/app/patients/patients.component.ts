import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { PatientService } from '../services/patient.service';
import { CaregiverService } from '../services/caregiver.service';
import { Patient } from '../data/patient';
import { Caregiver } from '../data/caregiver';
import { User } from '../data/user';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss'],
})
export class PatientsComponent implements OnInit {
  selectFormControl = new FormControl('', Validators.required);
  selectPatientFormControl = new FormControl('', Validators.required);

  patientForm = new FormGroup({
    nameCtrl: new FormControl('', [Validators.required]),
    dobCtrl: new FormControl('', [Validators.required]),
    genderCtrl: new FormControl('', [Validators.required]),
    addressCtrl: new FormControl('', [Validators.required]),
    medicalRecordCtrl: new FormControl('', [Validators.required]),
  });

  userForm = new FormGroup({
    usernameCtrl: new FormControl('', [Validators.required]),
    passwordCtrl: new FormControl('', [Validators.required]),
  });

  index = 1;

  displayedColumns: string[] = [
    'id',
    'name',
    'date of birth',
    'gender',
    'address',
    'medical record',
    'caregiver id',
    'caregiver name',
  ];
  dataSource = null;

  caregivers = null;

  selectedData;
  selectedPatient;

  hide = true;

  constructor(
    private router: Router,
    private patientService: PatientService,
    private caregiverService: CaregiverService
  ) {}

  ngOnInit(): void {
    this.patientForm.reset();
    this.userForm.reset();
    this.selectPatientFormControl.reset();
    this.selectFormControl.reset();
    this.selectedData = null;

    this.getAll();
    this.getAllCaregivers();
  }

  discard(): void {
    this.patientForm.reset();
    this.userForm.reset();
    this.selectPatientFormControl.reset();
    this.selectFormControl.reset();
  }

  getAll(): void {
    this.patientService.getAll().subscribe(
      (res) => {
        this.dataSource = res;
      },
      (error) => {}
    );
  }

  getAllCaregivers(): void {
    this.caregiverService.getAll().subscribe(
      (res) => {
        this.caregivers = res;
        console.log(this.caregivers);
      },
      (error) => {}
    );
  }

  save(): void {
    let caregiverPatient: Caregiver = null;

    if (this.selectedData !== null) {
      caregiverPatient = {
        id: this.selectedData.value,
        user: null,
        name: null,
        dob: null,
        gender: null,
        address: null,
      };
    }

    const userPatient: User = {
      username: this.userForm.controls.usernameCtrl.value,
      password: this.userForm.controls.passwordCtrl.value,
      type: null,
    };

    const patient: Patient = {
      id: null,
      user: userPatient,
      name: this.patientForm.controls.nameCtrl.value,
      dob: this.patientForm.controls.dobCtrl.value,
      gender: this.patientForm.controls.genderCtrl.value,
      address: this.patientForm.controls.addressCtrl.value,
      medicalRecord: this.patientForm.controls.medicalRecordCtrl.value,
      caregiver: caregiverPatient,
    };

    console.log(patient);

    this.patientService.create(patient).subscribe(
      (res) => {
        this.getAll();
        this.index = 1;
        this.patientForm.reset();
      },
      (error) => {}
    );
  }

  onTabChanged($event) {
    this.patientForm.reset();
    this.userForm.reset();
    this.selectPatientFormControl.reset();
    this.selectFormControl.reset();
  }

  selectedValue(event: MatSelectChange) {
    this.selectedData = {
      value: event.value,
      text: event.source.triggerValue,
    };
  }

  selectedPatientValue(event: MatSelectChange) {
    this.selectedPatient = {
      value: event.value,
      text: event.source.triggerValue,
    };
    console.log(this.selectedPatient);
  }

  delete(): void {
    this.patientService.delete(this.selectedPatient.value).subscribe(
      (res) => {
        this.getAll();
        this.selectPatientFormControl.reset();
        this.index = 1;
      },
      (error) => {}
    );
  }

  update(): void {
    let caregiverPatient: Caregiver = null;

    if (this.selectedData !== null) {
      caregiverPatient = {
        id: this.selectedData.value,
        user: null,
        name: null,
        dob: null,
        gender: null,
        address: null,
      };
    }

    const userPatient: User = {
      username: this.userForm.controls.usernameCtrl.value,
      password: this.userForm.controls.passwordCtrl.value,
      type: null,
    };

    const patient: Patient = {
      id: this.selectedPatient.value,
      user: userPatient,
      name: this.patientForm.controls.nameCtrl.value,
      dob: this.patientForm.controls.dobCtrl.value,
      gender: this.patientForm.controls.genderCtrl.value,
      address: this.patientForm.controls.addressCtrl.value,
      medicalRecord: this.patientForm.controls.medicalRecordCtrl.value,
      caregiver: caregiverPatient,
    };

    console.log(patient);

    this.patientService.update(patient).subscribe(
      res => {
        this.getAll();
        this.patientForm.reset();
        this.userForm.reset();
        this.selectPatientFormControl.reset();
        this.selectFormControl.reset();
        this.index = 1;
      },
      (error) => {
      }
    );
  }
}
