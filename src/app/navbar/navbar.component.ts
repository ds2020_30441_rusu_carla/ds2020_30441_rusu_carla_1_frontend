import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  user = '';
  subscription: Subscription;

  constructor(
    private router: Router,
    private messageService: MessageService,
    private authenticationService: AuthenticationService
  )
  {
    // subscribe to login component messages
    this.subscription = this.messageService.getMessage().subscribe(message => {
      if (message) {
        if (localStorage.getItem('type')) {
          this.user = localStorage.getItem('type');
        } else {
          this.user = '';
        }
      } else {
        // clear messages when empty message received
        this.user = '';
      }
    });
  }

  ngOnInit(): void {
    if (localStorage.getItem('type')) {
      this.user = localStorage.getItem('type');
    }
    else {
      this.user = '';
    }
  }

  login(): void {
    this.router.navigateByUrl('/login');
  }

  medication(): void {
    this.router.navigateByUrl('/medication');
  }

  patients(): void {
    this.router.navigateByUrl('/patients');
  }

  caregivers(): void {
    this.router.navigateByUrl('/caregivers');
  }

  plans(): void {
    this.router.navigateByUrl('/plans');
  }

  logout(): void {
    this.authenticationService.logout().subscribe(
      res => {
        localStorage.removeItem('token');
        localStorage.removeItem('type');
        this.router.navigateByUrl('');
        this.user = '';
      },
      (error) => {
      }
    );
  }

}
