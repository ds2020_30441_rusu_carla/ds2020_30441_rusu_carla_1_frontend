import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../data/user';
import { Token } from '../data/token';
import { AuthenticationService } from '../services/authentication.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  message = '';
  hide = true;
  token: Token;

  loginForm = new FormGroup({
    usernameCtrl: new FormControl('', [Validators.required]),
    passwordCtrl: new FormControl('', [Validators.required]),
  });

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
  }

  login(): void {

    const user: User   = {
      username:  this.loginForm.controls.usernameCtrl.value,
      password: this.loginForm.controls.passwordCtrl.value,
      type: null
    };
    this.authenticationService.login(user).subscribe(
      res => {
        this.token = res;

        localStorage.setItem('token', this.token.token);
        localStorage.setItem('type', this.token.type);

        if (this.token.type === 'doctor') {
          this.router.navigateByUrl('/medication');
        }

        if (this.token.type === 'patient') {
          this.router.navigateByUrl('/patient');
        }

        if (this.token.type === 'caregiver') {
          this.router.navigateByUrl('/caregiver');
        }

        this.messageService.sendMessage(this.token.type);
      },
      (error) => {
      }
    );

  }

  logout(): void {
    this.messageService.clearMessages();
  }

}
