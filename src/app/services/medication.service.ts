import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Medication } from '../data/medication';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/medication/`;

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  constructor(
    private http: HttpClient
  ) { }

  create(body: Medication): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}create`, body, httpOptions);
  }

  getAll(): Observable<Medication[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<Medication[]>(`${URL}get-all`, httpOptions);
  }

  update(body: Medication): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}update`, body, httpOptions);
  }

  delete(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.delete(`${URL}delete/${id}`, httpOptions);
  }
}
