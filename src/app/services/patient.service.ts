import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient } from '../data/patient';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/patient/`;

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(
    private http: HttpClient
  ) { }

  create(body: Patient): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}create`, body, httpOptions);
  }

  getAll(): Observable<Patient[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<Patient[]>(`${URL}get-all`, httpOptions);
  }

  delete(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.delete(`${URL}delete/${id}`, httpOptions);
  }

  update(body: Patient): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}update`, body, httpOptions);
  }

  getById(): Observable<Patient> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<Patient>(`${URL}by-id`, httpOptions);
  }

  getByCaregiver(): Observable<Patient[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<Patient[]>(`${URL}by-caregiver`, httpOptions);
  }
}
