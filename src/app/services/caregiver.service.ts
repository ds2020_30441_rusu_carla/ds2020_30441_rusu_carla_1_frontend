import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Caregiver } from '../data/caregiver';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/caregiver/`;

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  constructor(
    private http: HttpClient
  ) { }

  create(body: Caregiver): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}create`, body, httpOptions);
  }

  getAll(): Observable<Caregiver[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<Caregiver[]>(`${URL}get-all`, httpOptions);
  }

  delete(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.delete(`${URL}delete/${id}`, httpOptions);
  }

  update(body: Caregiver): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.post(`${URL}update`, body, httpOptions);
  }

  getById(): Observable<number> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<number>(`${URL}get-caregiver`, httpOptions);
  }
}
