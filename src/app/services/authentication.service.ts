import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../data/user';
import { Token } from '../data/token';
import { environment } from 'src/environments/environment';

const URL = `${environment.apiUrl}/authenticate/`;

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService implements CanActivate{

  constructor(
    private http: HttpClient,
    private route: Router
  ) { }

  // tslint:disable-next-line: max-line-length
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {

    let returnValue = true;
    if (localStorage.getItem('token') === null) {
      this.route.navigate(['']);
      return false;
    } else {
      if (localStorage.getItem('type') === 'doctor') {
        switch (route.url.toString()) {
          case 'patient':
            returnValue = false;
            this.route.navigate(['medication']);
            break;
          case 'caregiver' :
            returnValue = false;
            this.route.navigate(['medication']);
            break;
          case '' :
            returnValue = false;
            this.route.navigate(['medication']);
            break;
          case 'login' :
            returnValue = false;
            this.route.navigate(['medication']);
            break;
          default:
            returnValue = true;
            break;
        }
      }

      if (localStorage.getItem('type') === 'patient') {
          switch (route.url.toString()) {
            case 'patients':
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case 'caregivers' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case 'plans' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case 'medication' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case 'caregiver' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case '' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            case 'login' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            default:
              returnValue = true;
              break;
          }
        }

      if (localStorage.getItem('type') === 'caregiver') {
          switch (route.url.toString()) {
            case 'patients':
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case 'caregivers' :
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case 'plans' :
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case 'medication' :
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case 'patient' :
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case '' :
              returnValue = false;
              this.route.navigate(['caregiver']);
              break;
            case 'login' :
              returnValue = false;
              this.route.navigate(['patient']);
              break;
            default:
              returnValue = true;
              break;
          }
        }
    }

    return returnValue;
  }

  login(body: User): Observable<Token> {
    return this.http.post<Token>(`${URL}login`, body);
  }

  logout(): Observable<string> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token')
      })
    };

    return this.http.get<string>(`${URL}logout`, httpOptions);
  }
}
