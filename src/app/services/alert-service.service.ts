import { Injectable } from '@angular/core';
import { Alert } from '../data/alert';
import { environment } from 'src/environments/environment';

const serverUrl = `${environment.apiUrl}/socket`;
// Declare SockJS and Stomp
declare var SockJS;
declare var Stomp;

@Injectable({
  providedIn: 'root'
})
export class AlertServiceService {

  constructor() {
    this.initializeWebSocketConnection();
  }

  public stompClient;
  public msg = [];
  public alertDto: Alert;
  public caregiverId = 0;

  initializeWebSocketConnection(): void {
    // const serverUrl = 'http://localhost:8080/socket';
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;
    // tslint:disable-next-line:only-arrow-functions
    this.stompClient.connect({}, function(frame) {
      that.stompClient.subscribe('/alert', (message) => {
        this.alertDto = JSON.parse(message.body);
        that.caregiverId = this.alertDto.caregiverId;
        console.log(message);
        console.log(this.alertDto);
        console.log(this.caregiverId);

        that.msg.push(this.alertDto.msg);
      });
    });
  }

}
