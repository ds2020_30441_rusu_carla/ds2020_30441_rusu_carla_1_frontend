import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Patient, PatientSimple } from '../data/patient';
import { MedicationPlan } from '../data/plan';
import { environment } from 'src/environments/environment';

const URL = environment.apiUrl + '/plan/';

@Injectable({
  providedIn: 'root',
})
export class PlanService {
  constructor(private http: HttpClient) {}

  getAllByPatient(body: PatientSimple): Observable<MedicationPlan[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token'),
      }),
    };

    return this.http.post<MedicationPlan[]>(
      `${URL}by-patient`,
      body,
      httpOptions
    );
  }

  delete(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token'),
      }),
    };

    return this.http.delete(`${URL}delete/${id}`, httpOptions);
  }

  create(body: MedicationPlan): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        token: window.localStorage.getItem('token'),
      }),
    };

    return this.http.post(`${URL}create`, body, httpOptions);
  }
}
