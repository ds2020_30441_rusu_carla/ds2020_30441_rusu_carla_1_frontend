import { Caregiver } from './caregiver';
import { User } from './user';

export class Patient {
    id: number;
    user: User;
    name: string;
    dob: Date;
    gender: string;
    address: string;
    medicalRecord: string;
    caregiver: Caregiver;
}

export class PatientSimple {
    id: number;
}
