import { PatientSimple } from './patient';
import { User } from './user';

export class Caregiver {
    id: number;
    user: User;
    name: string;
    dob: Date;
    gender: string;
    address: string;
}

export class CaregiverPatients  {
    id: number;
    user: User;
    name: string;
    dob: Date;
    gender: string;
    address: string;
    patients: PatientSimple[];
}
