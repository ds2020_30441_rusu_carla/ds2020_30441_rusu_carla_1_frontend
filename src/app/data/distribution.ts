import { Medication } from './medication';

export class MedicationDistribution {
    id: number;
    intakeInterval: string;
    medication: Medication;
}
