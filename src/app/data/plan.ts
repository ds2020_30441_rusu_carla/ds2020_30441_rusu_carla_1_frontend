import { MedicationDistribution } from './distribution';
import { Patient } from './patient';

export class MedicationPlan {
    id: number;
    treatmentPeriod: string;
    patient: Patient;
    medicationDistributions: MedicationDistribution[];
}
