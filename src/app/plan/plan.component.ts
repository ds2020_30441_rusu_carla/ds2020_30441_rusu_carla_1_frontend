import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Medication } from '../data/medication';
import { MedicationService } from '../services/medication.service';
import { MatSelectChange } from '@angular/material/select';
import { PlanService } from '../services/plan.service';
import { PatientService } from '../services/patient.service';
import { Patient, PatientSimple } from '../data/patient';
import { MedicationPlan } from '../data/plan';
import { MedicationDistribution } from '../data/distribution';
import { element } from 'protractor';

@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss']
})
export class PlanComponent implements OnInit {
  selectPatientFormControl = new FormControl('', Validators.required);

  treatmentPeriodFormControl = new FormControl('');

  index = 1;

  selectedPatient;

  patients = null;

  displayedColumns: string[] = ['id', 'intake interval', 'med id', 'medication name', 'dosage', 'side effects'];
  dataSource = null;

  medicationNo: 1;
  medication = null;

  selectedMed: string;

  medicationForm: Array<FormGroup> = [
    new FormGroup({
      medicationCtrl: new FormControl('', [Validators.required]),
      intakeIntervalCtrl: new FormControl('', [Validators.required])
    })
  ];

  constructor(
    private router: Router,
    private medicationService: MedicationService,
    private planService: PlanService,
    private patientService: PatientService
  ) { }

  ngOnInit(): void {
    this.getAllPatients();
    this.getAllMedication();

    this.medicationForm.forEach(element => element.reset());
    this.treatmentPeriodFormControl.reset();
    this.selectPatientFormControl.reset();
  }

  getAllMedication(): void {
    this.medicationService.getAll().subscribe(
      (res) => {
        this.medication = res;
        console.log(this.medication);
      },
      (error) => {}
    );
  }

  addOneField(): void {
    this.medicationNo++;
    const medication = new FormGroup({
      medicationCtrl: new FormControl('', [Validators.required]),
      intakeIntervalCtrl: new FormControl('', [Validators.required])
    });
    this.medicationForm.push(medication);
  }

  minusOneField(): void {
    this.medicationNo--;
    this.medicationForm.pop();
  }

  getAllPatients() {
    this.patientService.getAll().subscribe(
      (res) => {
        this.patients = res;
        console.log(this.patients);
      },
      (error) => {}
    );
  }

  selectedPatientValue(event: MatSelectChange) {
    console.log(this.selectedPatient);

    const patient: PatientSimple = {
      id: this.selectedPatient
    };

    this.planService.getAllByPatient(patient).subscribe(
        (res) => {
          console.log(res);
          this.dataSource = res;
        },
        (error) => {}
      );
  }

  save(): void {
    const patientItem: Patient = {
      id: this.selectedPatient,
      user: null,
      name: null,
      dob: null,
      gender: null,
      address: null,
      medicalRecord: null,
      caregiver: null,
    };

    let medicationDistributionsItem: MedicationDistribution[] = [];

    if (this.medicationForm !== null) {
      this.medicationForm.forEach(element => {

        const medicationItem: Medication = {
          id: element.controls.medicationCtrl.value,
          name: null,
          sideEffects: null,
          dosage: null
        };

        const medicationDistribution: MedicationDistribution = {
          id: null,
          intakeInterval: element.controls.intakeIntervalCtrl.value,
          medication: medicationItem
        };

        medicationDistributionsItem.push(medicationDistribution);

      });
    }

    const medicationPlan: MedicationPlan = {
      id: null,
      treatmentPeriod: this.treatmentPeriodFormControl.value,
      patient: patientItem,
      medicationDistributions: medicationDistributionsItem
    };

    console.log(medicationPlan);

    this.planService.create(medicationPlan).subscribe(
      (res) => {
        this.getAllPatients();
        this.index = 1;
        this.medicationForm.forEach(element => element.reset());
        this.treatmentPeriodFormControl.reset();
        this.selectPatientFormControl.reset();
      },
      (error) => {}
    );
  }

  onTabChanged($event) {
    this.medicationForm.forEach(element => element.reset());
    this.treatmentPeriodFormControl.reset();
    this.selectPatientFormControl.reset();
  }

  discard(): void {
    this.medicationForm.forEach(element => element.reset());
    this.treatmentPeriodFormControl.reset();
    this.selectPatientFormControl.reset();
  }

  delete(plan: MedicationPlan): void {

    console.log(plan);

    this.planService.delete(plan.id).subscribe(
      (res) => {
        this.getAllPatients();
        this.index = 1;
        const patient: PatientSimple = {
          id: this.selectedPatient
        };
        this.planService.getAllByPatient(patient).subscribe(
            (res1) => {
              console.log(res);
              this.dataSource = res;
            },
            (error) => {}
          );
      },
      (error) => {}
    );
  }
}
