import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaregiverPageComponent } from './caregiver-page/caregiver-page.component';
import { CaregiversComponent } from './caregivers/caregivers.component';
import { LoginComponent } from './login/login.component';
import { MedicationComponent } from './medication/medication.component';
import { PatientPageComponent } from './patient-page/patient-page.component';
import { PatientsComponent } from './patients/patients.component';
import { PlanComponent } from './plan/plan.component';
import { AuthenticationService } from './services/authentication.service';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'medication',
    component: MedicationComponent,
    canActivate: [AuthenticationService]
  },
  {
    path: 'patients',
    component: PatientsComponent,
    canActivate: [AuthenticationService]
  },
  {
    path: 'caregivers',
    component: CaregiversComponent,
    canActivate: [AuthenticationService]
  },
  {
    path: 'plans',
    component: PlanComponent,
    canActivate: [AuthenticationService]
  },
  {
    path: 'patient',
    component: PatientPageComponent,
    canActivate: [AuthenticationService]
  },
  {
    path: 'caregiver',
    component: CaregiverPageComponent,
    canActivate: [AuthenticationService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
