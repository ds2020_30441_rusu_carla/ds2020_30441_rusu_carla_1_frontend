import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { PatientService } from '../services/patient.service';
import { CaregiverService } from '../services/caregiver.service';
import { Patient, PatientSimple } from '../data/patient';
import { Caregiver, CaregiverPatients } from '../data/caregiver';
import { User } from '../data/user';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-caregivers',
  templateUrl: './caregivers.component.html',
  styleUrls: ['./caregivers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class CaregiversComponent implements OnInit {
  selectCaregiverFormControl = new FormControl('', Validators.required);
  selectPatientsFormControl = new FormControl('', Validators.required);

  userForm = new FormGroup({
    usernameCtrl: new FormControl('', [Validators.required]),
    passwordCtrl: new FormControl('', [Validators.required]),
  });

  caregiverForm = new FormGroup({
    nameCtrl: new FormControl('', [Validators.required]),
    dobCtrl: new FormControl('', [Validators.required]),
    genderCtrl: new FormControl('', [Validators.required]),
    addressCtrl: new FormControl('', [Validators.required]),
  });

  index = 1;

  displayedColumns: string[] = [
    'id',
    'name',
    'date of birth',
    'gender',
    'address'
  ];
  dataSource = null;
  expandedElement: Caregiver | null;

  selectedCaregiver;
  selectedPatients: number[] = [];

  patients = null;

  hide = true;

  constructor(
    private router: Router,
    private patientService: PatientService,
    private caregiverService: CaregiverService
  ) { }

  ngOnInit(): void {
    this.userForm.reset();
    this.caregiverForm.reset();
    this.selectCaregiverFormControl.reset();
    this.selectPatientsFormControl.reset();

    this.getAll();
    this.getAllPatients();
  }

  getAllPatients() {
    this.patientService.getAll().subscribe(
      (res) => {
        this.patients = res;
        console.log(this.patients);

        this.patients = this.patients.filter(this.filterPatientsWithoutCaregiver);
        console.log(this.patients);

      },
      (error) => {}
    );
  }

  filterPatientsWithoutCaregiver(element: Patient, index, array) {
      return (element.caregiver === null);
  }

  discard(): void {
    this.userForm.reset();
    this.caregiverForm.reset();
    this.selectCaregiverFormControl.reset();
    this.selectPatientsFormControl.reset();
    this.selectedPatients = null;
  }

  getAll(): void {
    this.caregiverService.getAll().subscribe(
      (res) => {
        this.dataSource = res;
        console.log(this.dataSource);
      },
      (error) => {}
    );
  }

  onTabChanged($event) {
    this.userForm.reset();
    this.caregiverForm.reset();
    this.selectCaregiverFormControl.reset();
    this.selectPatientsFormControl.reset();
  }

  selectedCaregiverValue(event: MatSelectChange) {
    this.selectedCaregiver = {
      value: event.value,
      text: event.source.triggerValue,
    };
    console.log(this.selectedCaregiver);
  }

  delete(): void {
    this.caregiverService.delete(this.selectedCaregiver.value).subscribe(
      (res) => {
        this.getAll();
        this.userForm.reset();
        this.caregiverForm.reset();
        this.selectCaregiverFormControl.reset();
        this.selectPatientsFormControl.reset();
        this.index = 1;
      },
      (error) => {}
    );
  }

  save(): void {
    let caregiverPatients: PatientSimple[] = [];

    if (this.selectedPatients !== null) {
      if (this.selectedPatients.length !== 0) {
        this.selectedPatients.forEach(element => {
          const patient: PatientSimple = {
            id: element
          };
          caregiverPatients.push(patient);
        });
      }
    }

    if (caregiverPatients.length === 0) {
      caregiverPatients = null;
    }

    const userCaregiver: User = {
      username: this.userForm.controls.usernameCtrl.value,
      password: this.userForm.controls.passwordCtrl.value,
      type: null,
    };

    const caregiver: CaregiverPatients = {
      id: null,
      user: userCaregiver,
      name: this.caregiverForm.controls.nameCtrl.value,
      dob: this.caregiverForm.controls.dobCtrl.value,
      gender: this.caregiverForm.controls.genderCtrl.value,
      address: this.caregiverForm.controls.addressCtrl.value,
      patients: caregiverPatients
    };

    console.log(caregiver);

    this.caregiverService.create(caregiver).subscribe(
      (res) => {
        this.getAll();
        this.getAllPatients();
        this.index = 1;
        this.userForm.reset();
        this.caregiverForm.reset();
        this.selectCaregiverFormControl.reset();
        this.selectPatientsFormControl.reset();
      },
      (error) => {}
    );
  }

  update(): void {
    let caregiverPatients: PatientSimple[] = [];

    if (this.selectedPatients !== null) {
      if (this.selectedPatients.length !== 0) {
        this.selectedPatients.forEach(element => {
          const patient: PatientSimple = {
            id: element
          };
          caregiverPatients.push(patient);
        });
      }
    }

    if (caregiverPatients.length === 0) {
      caregiverPatients = null;
    }

    const userCaregiver: User = {
      username: this.userForm.controls.usernameCtrl.value,
      password: this.userForm.controls.passwordCtrl.value,
      type: null,
    };

    const caregiver: CaregiverPatients = {
      id: this.selectedCaregiver.value,
      user: userCaregiver,
      name: this.caregiverForm.controls.nameCtrl.value,
      dob: this.caregiverForm.controls.dobCtrl.value,
      gender: this.caregiverForm.controls.genderCtrl.value,
      address: this.caregiverForm.controls.addressCtrl.value,
      patients: caregiverPatients
    };

    console.log(caregiver);

    this.caregiverService.update(caregiver).subscribe(
      (res) => {
        this.getAll();
        this.getAllPatients();
        this.index = 1;
        this.userForm.reset();
        this.caregiverForm.reset();
        this.selectCaregiverFormControl.reset();
        this.selectPatientsFormControl.reset();
      },
      (error) => {}
    );
  }

}
