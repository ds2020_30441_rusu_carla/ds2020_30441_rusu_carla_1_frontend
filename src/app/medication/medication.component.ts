import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Medication } from '../data/medication';
import { MedicationService } from '../services/medication.service';
import { MatSelectChange } from '@angular/material/select';

@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.scss'],
})
export class MedicationComponent implements OnInit {
  selectFormControl = new FormControl('', Validators.required);

  medicationForm = new FormGroup({
    nameCtrl: new FormControl('', [Validators.required]),
    sideEffectsCtrl: new FormControl('', [Validators.required]),
    dosageCtrl: new FormControl('', [Validators.required]),
  });

  index = 1;

  displayedColumns: string[] = ['id', 'name', 'side effects', 'dosage'];
  dataSource = null;

  selectedData;

  constructor(
    private router: Router,
    private medicationService: MedicationService
  ) {}

  ngOnInit(): void {
    this.medicationForm.reset();
    this.selectFormControl.reset();
    this.getAll();
  }

  save(): void {
    const medication: Medication = {
      id: null,
      name: this.medicationForm.controls.nameCtrl.value,
      sideEffects: this.medicationForm.controls.sideEffectsCtrl.value,
      dosage: this.medicationForm.controls.dosageCtrl.value,
    };

    this.medicationService.create(medication).subscribe(
      (res) => {
        this.getAll();
        this.index = 1;
        this.medicationForm.reset();
      },
      (error) => {}
    );
  }

  update(): void {
    const medication: Medication = {
      id: this.selectedData.value,
      name: this.medicationForm.controls.nameCtrl.value,
      sideEffects: this.medicationForm.controls.sideEffectsCtrl.value,
      dosage: this.medicationForm.controls.dosageCtrl.value,
    };

    this.medicationService.update(medication).subscribe(
      (res) => {
        this.getAll();
        this.medicationForm.reset();
        this.index = 1;
      },
      (error) => {}
    );

    console.log(medication);
    this.medicationForm.reset();
  }

  onTabChanged($event) {
    this.medicationForm.reset();
    this.selectFormControl.reset();
  }

  selectedValue(event: MatSelectChange) {
    this.selectedData = {
      value: event.value,
      text: event.source.triggerValue,
    };
    console.log(this.selectedData);
  }

  discard(): void {
    this.medicationForm.reset();
    this.selectFormControl.reset();
  }

  getAll(): void {
    this.medicationService.getAll().subscribe(
      (res) => {
        this.dataSource = res;
      },
      (error) => {}
    );
  }

  delete(): void {
    this.medicationService.delete(this.selectedData.value).subscribe(
      (res) => {
        this.getAll();
        this.selectFormControl.reset();
        this.index = 1;
      },
      (error) => {}
    );
  }
}
