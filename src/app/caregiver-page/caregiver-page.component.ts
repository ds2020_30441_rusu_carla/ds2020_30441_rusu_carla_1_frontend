import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Caregiver } from '../data/caregiver';
import { Patient, PatientSimple } from '../data/patient';
import { User } from '../data/user';
import { PatientService } from '../services/patient.service';
import { PlanService } from '../services/plan.service';
import { FormControl, Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { AlertServiceService } from '../services/alert-service.service';
import { CaregiverService } from '../services/caregiver.service';

@Component({
  selector: 'app-caregiver-page',
  templateUrl: './caregiver-page.component.html',
  styleUrls: ['./caregiver-page.component.scss']
})
export class CaregiverPageComponent implements OnInit {

  displayedColumns: string[] = ['id', 'intake interval', 'med id', 'medication name', 'dosage', 'side effects'];
  dataSource = null;

  patients: Patient[];

  selectPatientFormControl = new FormControl('', Validators.required);

  selectedPatient;

  id;

  constructor(
    private router: Router,
    private patientService: PatientService,
    private planService: PlanService,
    public alertService: AlertServiceService,
    private caregiverService: CaregiverService
  ) { }

  ngOnInit(): void {
    this.getPatients();
    this.getCaregiver();
  }

  getCaregiver(): void {
    this.caregiverService.getById().subscribe(
      (res) => {
        this.id = res;
        console.log(this.id);
      },
      (error) => {}
    );
  }

  getPatients(): void {
    this.patientService.getByCaregiver().subscribe(
      (res) => {
        this.patients = res;
        console.log(this.patients);
      },
      (error) => {}
    );
  }

  selectedPatientValue(event: MatSelectChange) {
    console.log(this.selectedPatient);

    const patient: PatientSimple = {
      id: this.selectedPatient
    };

    this.planService.getAllByPatient(patient).subscribe(
      (res) => {
        console.log(res);
        this.dataSource = res;
      },
      (error) => {}
    );
  }

}
