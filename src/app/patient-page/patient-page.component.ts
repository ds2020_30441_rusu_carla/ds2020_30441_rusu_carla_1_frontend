import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Caregiver } from '../data/caregiver';
import { Patient } from '../data/patient';
import { User } from '../data/user';
import { PatientService } from '../services/patient.service';
import { PlanService } from '../services/plan.service';

@Component({
  selector: 'app-patient-page',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.scss']
})
export class PatientPageComponent implements OnInit {

  patient: Patient = new Patient();
  displayedColumns: string[] = ['id', 'intake interval', 'med id', 'medication name', 'dosage', 'side effects'];
  dataSource = null;

  constructor(
    private router: Router,
    private patientService: PatientService,
    private planService: PlanService
  ) { }

  ngOnInit(): void {
    this.patient.caregiver = new Caregiver();
    this.patient.user = new User();
    this.patient.dob = null;

    this.getPatient();
  }

  getPlans(): void {
    this.planService.getAllByPatient(this.patient).subscribe(
      (res) => {
        console.log(res);
        this.dataSource = res;
      },
      (error) => {}
    );
  }

  getPatient(): void {
    this.patientService.getById().subscribe(
      (res) => {
        this.patient = res;
        console.log(this.patient);
      },
      (error) => {}
    );

    setTimeout(() => {
      this.getPlans();
    }, 1000);
  }

}
